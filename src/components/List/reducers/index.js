import { createReducer } from 'redux-act'
import * as listActions from './../actions'

let initialState = {
  isFetching: true,
  maxComments: null,
  listCards: []
}
const reducers = createReducer({
  [listActions.getCards]: (state, payload) => {
    return {
      ...state,
      ...payload,
      maxComments: Math.max.apply(Math,payload.listCards.map(o => o.data.num_comments)),
      isFetching: false
    }
  }
}, initialState)

export default reducers