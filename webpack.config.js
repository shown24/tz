const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const WriteFilePlugin = require('write-file-webpack-plugin')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')

const srcPath = path.join(__dirname, '/src')
const distPath = path.join(__dirname, '/dist')

const isProd = process.env.NODE_ENV === 'production'

module.exports = {
  watch: !isProd,
  devtool: 'source-map',
  context: srcPath,
  entry: {
    app: './index.js',
  },
  output: {
    path: distPath,
    filename: '[name].bundle.js',
    publicPath: path.join(__dirname, '/')
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: ["node_modules"]
  },
  plugins: (() => {
    let plusins = [
      new webpack.NoEmitOnErrorsPlugin(),
      new HtmlWebpackPlugin({
        template: './index.html',
        inject: 'body'
      }),
      new WriteFilePlugin(),
      new webpack.HotModuleReplacementPlugin()
    ]
    if (isProd) {
      plusins.push(new UglifyJSPlugin({
        compress: {
          warnings: false
        }
      }))
    }
    return plusins
  })(),

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoader: 1,
              localIdentName: '[path]___[name]__[local]___[hash:base64:5]'
            }
          }]
      },
      {
        test: /\.js$/,
        use: [
          'babel-loader'
        ],
        exclude: /node_modules/
      },
      {
        test: /\.jsx$/,
        use: [
          'babel-loader'
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.html$/,
        use: [
          'html-loader',
        ],
      },
    ]
  },
  devServer: {
    historyApiFallback: true,
    hot: true,
    contentBase: __dirname + '/dist',
    publicPath: ''
  }
};