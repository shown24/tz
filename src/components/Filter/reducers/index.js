import { createReducer } from 'redux-act'
import * as filterActions from './../actions'

let initialState = {
  typeSort: 'comments',
  sortByCommentsNumber: 0,
  sortByDate: 'new'
}

const updateState = (state, payload) => {
  return {
    ...state,
    ...payload
  }
}

const reducers = createReducer({
  [filterActions.sortByComments]: updateState,
  [filterActions.changeSortType]: updateState,
  [filterActions.sortByDate]: updateState
}, initialState)

export default reducers