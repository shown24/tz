import React from 'react'

import CSSModules from 'react-css-modules';
import styles from './index.css'

import Title from './../../components/Title'
import Slider from '../../components/Filter'
import List from '../../components/List'

class Home extends React.Component {
  render () {
    return <div styleName='container'>
      <Title />
      <div styleName='list'>
        <Slider />
        <List />
      </div>
    </div>
  }
}

export default CSSModules(Home, styles)