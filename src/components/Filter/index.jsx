import React from 'react'
import { connect } from 'react-redux'

import * as filterActions from './actions'

import styles from './index.css'
import CSSModules from 'react-css-modules'

class Filter extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      inputValue: 0
    }
  }

  onChange (e) {
    filterActions.sortByComments(e.target.value)
    this.setState({inputValue: e.target.value})
  }

  onChangeTypeSort (e) {
    filterActions.changeSortType(e.target.dataset.type)
  }

  oldNewCards (e) {
    filterActions.sortByDate(e.target.dataset.date)
  }

  render () {
    return <div styleName='block'>
      <div>
        {
          this.props.typeSort === 'comments'
            ? <div styleName='button' data-type='date' onClick={this.onChangeTypeSort}>Sort by Date</div>
            : <div styleName='button' data-type='comments' onClick={this.onChangeTypeSort}>Sort by Comments</div>
        }
      </div>
      {
        this.props.typeSort === 'comments' ?
          <div>
            <div styleName='info'>Comments {this.state.inputValue}</div>
            <input styleName="slider" onChange={this.onChange.bind(this)} value={this.state.inputValue} type="range"
                   min="0"
                   max={this.props.maxCommentsValue + 1} step="1"/>
          </div> :
          <div styleName='nav'>
            <div data-date='new' onClick={this.oldNewCards} styleName='button2'
                 className={this.props.sortByDate === 'new' ? 'active' : ''}>New
            </div>
            <div data-date='old' onClick={this.oldNewCards} styleName='button2'
                 className={this.props.sortByDate === 'old' ? 'active' : ''}>Old
            </div>
          </div>
      }
    </div>
  }
}
function mapStateToProps ({Filter, List}) {
  return {
    typeSort: Filter.typeSort,
    sortByDate: Filter.sortByDate,
    maxCommentsValue: List.maxComments
  }
}

export default connect(mapStateToProps)(CSSModules(Filter, styles))