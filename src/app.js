import React from 'react'

import Router from './routes'

import './app.css'

const App = () => <div className="App"><Router /></div>
export default App