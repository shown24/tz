import { createStore, combineReducers } from 'redux'
import {assignAll} from 'redux-act'

import * as filterActions from './../components/Filter/actions'
import * as listActions from './../components/List/actions'

import Filter from './../components/Filter/reducers'
import List from './../components/List/reducers'

const storeApp = combineReducers({
  Filter,
  List
})

let store = createStore(storeApp)
assignAll(filterActions, store)
assignAll(listActions, store)

export default store