import React from 'react'
import styles from './index.css'
import CSSModules from 'react-css-modules';

const Title = () => <div styleName='title'>Gallery!!!</div>

export default CSSModules(Title, styles)