import { createAction } from 'redux-act'

export const getCards = createAction('getCards', ({data}) => ({listCards: data.children}))