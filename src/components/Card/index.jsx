import React from 'react'
import CSSModules from 'react-css-modules';

import styles from './index.css'

const Card = props => <div styleName='card'>
  <div styleName='img'><img alt='' src={props.thumbnail} /></div>
  <div styleName='date'>{(new Date(props.created * 1000)).toLocaleString()}</div>
  <div styleName='title'>{props.title}</div>
  <div styleName='comments'>Comments {props.num_comments}</div>
  <a styleName='link' href={`https://www.reddit.com${props.permalink}`} target='_blank'>Link</a>
</div>

export default CSSModules(Card, styles)