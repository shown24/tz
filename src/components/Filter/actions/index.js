import {createAction} from 'redux-act'

export const sortByComments = createAction('sortByComments', (number) => ({sortByCommentsNumber: number}))
export const changeSortType = createAction('changeSortType', (type) => ({typeSort: type}))
export const sortByDate = createAction('changeSortType', (type) => ({sortByDate: type}))