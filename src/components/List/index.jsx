import React from 'react'
import { connect } from 'react-redux'

import CSSModules from 'react-css-modules';
import api from 'axios'

import styles from './index.css'

import * as listActions from './actions'

import Card from '../Card/index'

class List extends React.Component {

  componentDidMount () {
    api.get('https://www.reddit.com/r/aww.json').then(({data}) => {
      listActions.getCards(data)
    })
  }

  sortBy (type) {
    let sortFuncs = {
      comments: (a, b) => a.data.num_comments < b.data.num_comments ? 1
        : a.data.num_comments > b.data.num_comments ? -1
          : 0,

      date: (a, b) => {
        let result = null
        if (this.props.sortByDate === 'new') {
          result = a.data.created < b.data.created ? 1
            : a.data.created > b.data.created ? -1
              : 0
        } else {
          result = a.data.created < b.data.created ? -1
            : a.data.created > b.data.created ? 1
              : 0
        }
        return result
      }
    }
    return sortFuncs[type]
  }

  sortList (list) {
    return list
      .sort(this.sortBy(this.props.typeSort))
      .filter((el) => (el.data.num_comments >= this.props.sortByCommentsNumber))
      .map(el => <Card key={`card_${el.data.id}`} {...el.data} />)
  }

  render () {
    let list = this.sortList(this.props.listCards)

    return this.props.isFetching
      ? <div>Loading...</div>
      : <div styleName='list'>
        {
          list.length
            ? list
            : <div>No results found matching your criteria</div>
        }
      </div>
  }
}

function mapStateToProps ({List, Filter}) {
  return {
    listCards: List.listCards,
    isFetching: List.isFetching,
    sortByCommentsNumber: Filter.sortByCommentsNumber,
    typeSort: Filter.typeSort,
    sortByDate: Filter.sortByDate
  }
}

export default connect(mapStateToProps)(CSSModules(List, styles))